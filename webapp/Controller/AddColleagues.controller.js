sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "zhcm_Hap_650/Utils/Config",
    "zhcm_Hap_650/Utils/DataAccess",
    "zhcm_Hap_650/Utils/Formatter",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Filter",
    "sap/m/Token",
    "sap/m/Tokenizer"
], function (JSONModel, Controller, Config, DataAccess, Formatter, MessageToast, MessageBox, Filter, Token, Tokenizer) {
    "use strict";
    return Controller.extend("zhcm_Hap_650.Controller.AddColleagues", {
        formatter: Formatter,
        sAppraisalId: "",

        onInit: function () {
            // #DEBUG START
            // if (window.location.hostname.toLowerCase() === "sms00990" ||
            //     window.location.hostname === "127.0.0.1" ||
            //     window.location.hostname.toLowerCase() === "localhost") window.goControllerAc = this;
            // #DEBUG END

            var mViewData = {
                HeaderExpanded: true,
                ColleaguesNum: ""
            };

            var oViewDataModel = new JSONModel(mViewData);
            this.getView().setModel(oViewDataModel, "viewData");

            this.getView().setModel(new JSONModel(), "SearchResults");
            this.getView().setModel(new JSONModel(), "AddedColleagues");
            this.getView().setModel(new JSONModel(), "OrgehValueHelpSet");
            this.getOwnerComponent().getRouter().getRoute("addColleagues")
                .attachMatched(this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("maddColleagues")
                .attachMatched(this._onRouteMatched, this);

        },

        _onRouteMatched: function (oEvt) {
            var sPenrMemId = this.getOwnerComponent().getModel("AppData").getProperty("/CurrentPernrMemId"),
            oEmployeesDataDef = new $.Deferred();
            
            this.getView().setBusy(true);
            this.sAppraisalId = oEvt.getParameter("arguments").AppraisalId;
            this._setViewAvailability(oEvt.getParameter("arguments").IndicatorId);

            DataAccess.getAddedEmployees(this.sAppraisalId, sPenrMemId)
                .then(this._parseBackendResponse.bind(this, oEmployeesDataDef));

            oEmployeesDataDef.then(function (mEmployeeData) {
                if (mEmployeeData.LOCKED) sap.m.MessageBox.warning(mEmployeeData.LOC_MSG);
            });
        },

        _parseBackendResponse: function(oEmployeesDataDef, oEmployees){
            var aColleaguesMaped = {},
                oViewData = this.getView().getModel("viewData"),
                sHdrInstructionText = DataAccess.getInstrText.call(this, "AD", "HEADER", oEmployees.ID_EVAL_PROC),
                sInstructionText = DataAccess.getInstrText.call(this, "AD", "BTN_POPUP", oEmployees.ID_EVAL_PROC);

            oViewData.setProperty("/isApprover", oEmployees.IS_APPROVER);
            oViewData.setProperty("/IdEvalProc", oEmployees.ID_EVAL_PROC);
            oViewData.setProperty("/CollApprove", oEmployees.COLL_APPROVE);
            oViewData.setProperty("/AddInfoText", sHdrInstructionText);
            oViewData.setProperty("/InstructionButton", sInstructionText);

            $.each(oEmployees.EmplSetTableSet.results, function (iInd, mEmpl) {
                delete mEmpl.__metadata;
                aColleaguesMaped[mEmpl.PERNR] = mEmpl;
            });

            this.getView().getModel("AddedColleagues").setData(aColleaguesMaped);

            var headerData = $.extend({}, oEmployees);
            delete headerData.EmplSetTableSet;
            this.getView().setModel(new JSONModel(headerData), "Person");

            this._createCollegNumHTML(parseInt(oEmployees.COLL_MIN), parseInt(oEmployees.COLL_MAX));

            oEmployeesDataDef.resolve(headerData);
            this.getView().setBusy(false);
        },

        _setViewAvailability: function (sTaskStatus) {
            var bActionsEnabled = false;
            if (sTaskStatus) {
                bActionsEnabled = Config.taskStatusClosed.indexOf(sTaskStatus) > -1 ? false : true;
            }
            this.getView().getModel("viewData").setProperty("/ViewActionsEnabled", bActionsEnabled);
        },

        _createCollegNumHTML: function (iMin, iMax) {
            var collNumText = "Выберите ### коллег для участия в оценке",
                sReplaceText = "";
            if (iMin) {
                sReplaceText = " от " + "<span class='zBlueTextBold'>" + iMin + "</span>";
            }
            if (iMax) {
                sReplaceText += " до " + "<span class='zBlueTextBold'>" + iMax + "</span>";
            }
            this.getView().getModel("viewData").setProperty("/ColleaguesNum", collNumText.replace('###', sReplaceText));
        },

        showUserGuide: function (oEvt) {

            if (!this._oPopover) {
                // перенесено в _onRouteMatched
                // var oViewDataModel = this.getView().getModel("viewData"),
                //     sIdEvalProc = oViewDataModel.getProperty("/IdEvalProc"),
                //     sInstructionText = DataAccess.getInstrText.call(this, 'AD', 'BTN_POPUP', sIdEvalProc);

                // oViewDataModel.setProperty("/InstructionButton", sInstructionText);

                this._oPopover = sap.ui.xmlfragment("zhcm_Hap_650.fragments.UserGuidePopover", this);
                this.getView().addDependent(this._oPopover);
                this._oPopover.attachAfterClose(function () {
                    this._oPopover.destroy();
                    this._oPopover = null;
                }, this);

                this._oPopover.openBy(oEvt.getSource());
            } else {
                this._oPopover.close();
            }
        },

        onDeleteAdded: function (oEvt) {
            var oBindingContext = oEvt.getParameter("employeeItem").getBindingContext("AddedColleagues");
            var sPernr = oBindingContext.getObject().PERNR;
            var oBundle = this.getView().getModel("i18n").getResourceBundle();

            // show confirmation dialog
            MessageBox.show(oBundle.getText("ColleaguesDeleteDialogMsg"), {
                title: oBundle.getText("ColleaguesDeleteDialogTitle"),
                styleClass: "zCustomMessageBox",
                actions: [
                    MessageBox.Action.DELETE,
                    MessageBox.Action.CANCEL
                ],
                onClose: function (oAction) {
                    if (oAction !== MessageBox.Action.DELETE) {
                        return;
                    }
                    var oColleaguesModel = oBindingContext.getModel();
                    var oEntries = $.extend({}, oColleaguesModel.getData());

                    delete oEntries[sPernr];

                    // update model
                    oColleaguesModel.setProperty("/", $.extend({}, oEntries));
                }
            });
        },

        onSearchColleagues: function (oEvt) {
            var oSearchField,
                // oCloseButton,
                oMultiInput;

            if (!this._oSearchEmployeeDialog) {
                this._oSearchEmployeeDialog = sap.ui.xmlfragment("SEDid", "zhcm_Hap_650.fragments.SearchEmployeeDialog", this);
                this.getView().addDependent(this._oSearchEmployeeDialog);

            }
            this.getView().getModel("SearchResults").setData(null);
            this._addedViaDialogNum = 0;

            // oCloseButton = sap.ui.core.Fragment.byId("SEDid", "idSearchEmployeeDialogCloseButton");
            oMultiInput = sap.ui.core.Fragment.byId("SEDid", "minp_shlp_orgeh");
            oSearchField = sap.ui.core.Fragment.byId("SEDid", "fioSearchField");
            oSearchField.setValue("");
            oMultiInput.setTokens([]);

            // необходимо снимать фокус с элементов ввода при открытии диалога
            // ждем 1 кадр, чтобы появился html контрола
            $.sap.delayedCall(0, null, function () {
                oSearchField.$().find("input").get(0).setAttribute("tabindex", "-1");
                oMultiInput.$().find("input").get(0).setAttribute("tabindex", "-1");

                // почему-то не работает, видимо фокус на серч филд выставляется сильно позже открытия.
                // oCloseButton.focus();
            });

            this._oSearchEmployeeDialog.open();
        },

        handleSearch: function (oEvt) {
            var regEx1 = /^[ \t]+/g, // убирает пробелы в начале
                regex2 = /\s\s+/g, // неск. пробелов в 1
                sSearchString = oEvt.getParameter("query").replace(regEx1, "").replace(regex2, " "),
                that = this,
                sORGEH = this._sSelectedORGEH; // заполняется при закрытии СП по оргединицам

            if (sSearchString) {
                this._oSearchEmployeeDialog.setBusy(true);
                DataAccess.searchForEmpl(sSearchString, this.sAppraisalId, sORGEH).then(
                    function (oObj) {
                        that.getView().getModel("SearchResults").setData(oObj);
                        // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                        that._setTabRowsDisabled(oObj);
                        that._oSearchEmployeeDialog.setBusy(false);
                    },
                    function (oError) {
                        that.getView().getModel("SearchResults").setData(null);
                        that._oSearchEmployeeDialog.setBusy(false);
                        MessageBox.error(oError.error.message.value);
                    });
            }
        },

        handleSelect: function (oEvt) {
            var oContext = oEvt.getSource().getBindingContext("SearchResults"),
                oSelectedEmployee = oContext.getModel().getProperty(oContext.getPath()),
                oColleaguesModel = this.getView().getModel("AddedColleagues");

            this._addToColleaguesList(oSelectedEmployee, oColleaguesModel);
        },

        _addToColleaguesList: function (oColleagueToBeAdded, oColleaguesModel) {
            // find existing
            var oCollectionEntries = $.extend({}, oColleaguesModel.getData());
            var oListEntry = oCollectionEntries[oColleagueToBeAdded.PERNR];

            if (oListEntry === undefined) {
                // create new entry
                oListEntry = $.extend({}, oColleagueToBeAdded);

                if(oListEntry.DISABLED){
                    var str = "Этот сотрудник уже участвует в оценке";
                    if (oListEntry.hasOwnProperty("DISABLED_MESSAGE") && oListEntry.DISABLED_MESSAGE.length > 0) {
                        str = oListEntry.DISABLED_MESSAGE;
                    }
                    MessageToast.show(str);
                    return;
                }

                // Проверка на соответветствие мнимальному стажу выбранного сотрудника
                var sPersonData = this.getView().getModel("Person").getData();
                if (Number(oListEntry.DATE) >= Number(sPersonData.MIN_EXP)) {
                    delete oListEntry.__metadata;
                    delete oListEntry.APPRAISALID;
                    delete oListEntry.DATE;
                    delete oListEntry.DISABLED;
                    oCollectionEntries[oColleagueToBeAdded.PERNR] = oListEntry;

                    //update model
                    oColleaguesModel.setProperty("/", $.extend({}, oCollectionEntries));
                    oColleaguesModel.refresh(true);
                    this._addedViaDialogNum += 1;
                    MessageToast.show("Добавлен");
                }
                else {
                    MessageToast.show("Стаж сотрудника менее необходимого количества дней: " + Number(sPersonData.MIN_EXP) + ". Недоступен для выбора.");
                }
            } else {
                // already in the list
                MessageToast.show("Этот сотрудник уже в списке");
            }
        },

        handleClose: function (oEvt) {
            var that = this;
            if (!this._addedViaDialogNum) {
                MessageBox.show("Коллеги не выбраны. Вы уверены, что хотите закрыть поиск?", {
                    title: "Закрыть",
                    styleClass: "zCustomMessageBox",
                    actions: [MessageBox.Action.YES, MessageBox.Action.CANCEL],
                    onClose: function (oAction) {
                        if (oAction === MessageBox.Action.YES) {
                            that._oSearchEmployeeDialog.close();
                            that.getView().setBusy(false);
                        }
                    }
                });
            } else {
                this._oSearchEmployeeDialog.close();
                this.getView().setBusy(false);
            }
        },

        onSaveList: function (oEvt) {
            this._sendList("");
        },

        onSendToApprove: function (oEvt) {
            this._sendList("SEND");
        },

        onSendNoApprove: function (oEvt) {
            this._sendList("COMPLETE");
        },

        onApprove: function (oEvt) {
            this._sendList("APPR");
        },

        onBtnBackPress: function (oEvt) {
            if(this.getOwnerComponent().getModel("device").getProperty('/isPhone')){
                this.getOwnerComponent().getRouter().navTo("mOverview");
            } else {
                this.getOwnerComponent().getRouter().navTo("overview");
            }
        },

        _sendList: function (sAction) {
            this.getView().setBusy(true);
            var that = this;
            var aAdded = $.extend(this.getView().getModel("AddedColleagues").getData());
            var aAddedParsed = Object.keys(aAdded).map(function (sKey) {
                return aAdded[sKey];
            });

            aAddedParsed.forEach(function (oColleague) {
                delete oColleague.DISABLED_MESSAGE;
            });

            var sendData = {
                APPRAISAL_ID: this.sAppraisalId,
                BUTTON: sAction,
                EmplSetTableSet: aAddedParsed
            };

            DataAccess.sendEmployeeList(sendData)
                .then(function (oRes) {
                        that.getView().setBusy(false);
                        if (sendData.BUTTON != "") {
                            that.getOwnerComponent().getRouter().navTo("overview");
                        }
                        MessageToast.show("Сохранено");
                    },
                    function (oError) {
                        that.getView().setBusy(false);
                        MessageBox.error(oError.error.message.value);
                    });

        },

        handleValueHelpORGEH: function (oEvt) {
            if (!this._oValueHelpDialog) {
                this._oValueHelpDialog = sap.ui.xmlfragment("idOrgehDialog", "zhcm_Hap_650.fragments.SearchOrgehDialog", this);
                this.getView().addDependent(this._oValueHelpDialog);
            }

            var oOrgDialog = sap.ui.core.Fragment.byId("idOrgehDialog", "sld_ogreh"),
                oBinding = oOrgDialog.getBinding("items"),
                that = this;

            // Очищаем фильтр в средстве поиска оргедниц
            oBinding.filter([]);

            oOrgDialog._getCancelButton().addStyleClass("zhcm_Hap_650CloseBtn");
            oOrgDialog._getCancelButton().setType("Unstyled");
            this._oSearchEmployeeDialog.setBusy(true);

            DataAccess.get_sf_orgeh(this.sAppraisalId).then(
                function (oObj) {
                    that.getView().getModel("OrgehValueHelpSet").setData(oObj);
                    that._oSearchEmployeeDialog.setBusy(false);
                    that._oValueHelpDialog.open();
                },
                function (oError) {
                    that._oSearchEmployeeDialog.setBusy(false);
                });
        },

        handleSearchORGEH: function (oEvt) {
            var sValue = oEvt.getParameter("value"),
                oFilter = new Filter("ORGEHTXT", sap.ui.model.FilterOperator.Contains, sValue),
                oBinding = oEvt.getSource().getBinding("items");

            oBinding.filter([oFilter]);
        },

        handleConfirmORGEH: function (oEvt) {
            var oSelectedItem = oEvt.getParameter("selectedItem"),
                sFioInput = sap.ui.core.Fragment.byId("SEDid", "fioSearchField").getValue(),
                that = this;

            if (oSelectedItem) {
                var sORGEHTXT = oSelectedItem.getTitle(),
                    sORGEH = oSelectedItem.getDescription();
                this._sSelectedORGEH = sORGEH; // заполняется для использования в методе handleSearch

                var oInputOrgeh = sap.ui.core.Fragment.byId("SEDid", "minp_shlp_orgeh");
                oInputOrgeh.setTokens([new Token({key: sORGEHTXT, text: sORGEHTXT})]);

                if (sFioInput) {
                    this._oSearchEmployeeDialog.setBusy(true);
                    DataAccess.searchForEmpl(sFioInput, this.sAppraisalId, sORGEH).then(
                        function (oObj) {
                            that.getView().getModel("SearchResults").setData(oObj);
                            // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                            that._setTabRowsDisabled(oObj);
                            that.getView().setBusy(false);
                            that._oSearchEmployeeDialog.setBusy(false);
                        },
                        function (oError) {
                            that.getView().getModel("SearchResults").setData(null);
                            that._oSearchEmployeeDialog.setBusy(false);
                            MessageBox.error(oError.error.message.value);
                        });
                }

                // в ИЕ при очень длинном тексте токена диалог уезжает влево за край экрана. Чиним.
                $.sap.delayedCall(0, null, function () {
                    that._oSearchEmployeeDialog.rerender();
                });
            }
        },

        _setTabRowsDisabled: function (oObj) {
            var sPersonData = this.getView().getModel("Person").getData(),
                oTabResults = sap.ui.core.Fragment.byId("SEDid", "tblColleaguesResults"),
                aTabItems = oTabResults.getItems(),
                oAdded = this.getView().getModel("AddedColleagues").getData();

            // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
            for (var i = 0; i < aTabItems.length; i++) {
                // Проверка на соответветствие мнимальному стажу выбранного сотрудника
                // или - коллега уже добавлен, но не сохранен. Тоже серым
                if (Number(oObj[i].DATE) >= Number(sPersonData.MIN_EXP) &&
                    !oAdded.hasOwnProperty(oObj[i].PERNR) &&
                    !oObj[i].DISABLED) {
                    aTabItems[i].removeStyleClass("zhcm_Hap_650DisableText");
                    aTabItems[i].addStyleClass("zhcm_Hap_650EnableText");
                }
                else {
                    aTabItems[i].removeStyleClass("zhcm_Hap_650EnableText");
                    aTabItems[i].addStyleClass("zhcm_Hap_650DisableText");
                }
            }
        },

        onTokenUpdate_minp_shlp_orgeh: function (oEvt) {
            if (oEvt.getParameter("type") === Tokenizer.TokenUpdateType.Removed) {
                var sSF_FIO = sap.ui.core.Fragment.byId("SEDid", "fioSearchField").getValue(),
                    that = this;

                this._sSelectedORGEH = ""; // заполняется для использования в методе handleSearch
                if (sSF_FIO) {
                    this._oSearchEmployeeDialog.setBusy(false);
                    DataAccess.searchForEmpl(sSF_FIO, this.sAppraisalId, null)
                        .then(
                            function (oObj) {
                                that.getView().getModel("SearchResults").setData(oObj);
                                // Устанавливаем стиль для строки таблицы в зависимости от стажа сотрудника
                                that._setTabRowsDisabled(oObj);
                                that._oSearchEmployeeDialog.setBusy(false);
                            },
                            function (oError) {
                                that.getView().getModel("SearchResults").setData(null);
                                that._oSearchEmployeeDialog.setBusy(false);
                                MessageBox.error(oError.error.message.value);
                            });
                }
            }
        },

        onToggleHeader: function () {
            var oViewData = this.getView().getModel("viewData");
            oViewData.setProperty("/HeaderExpanded", !oViewData.getProperty("/HeaderExpanded"));
        },

        onItemsBindingChangeColleaguesGrid: function (oEvent) {
            var oColleaguesGrid = this.getView().byId("colleaguesGrid");
            var aItems = oColleaguesGrid.getItems();
            
            if (aItems.length > 0) {
                // ждем 1 кадр, пока карточки появятся в DOM
                $.sap.delayedCall(0, null, function () {
                    // удаляем результат старого выставления ширины
                    aItems.forEach(function (oItem) {
                        oItem.$().css("display", "block");
                        oItem.$().height("auto");
                    });

                    $.sap.delayedCall(0, null, function () {
                        aItems.forEach(function (oItem) {
                            oItem.$().css("display", "flex");
                        });
                        
                        // вычисляем высоту самой высокой карточки
                        // var iMaxHeight = aItems.reduce(function (iCurMaxHeight, oItem) {
                        //     var iCurItemHeight = oItem.getDomRef().parentElement.clientHeight;
                        //     return Math.max(iCurItemHeight, iCurMaxHeight);
                        // }, 0);
                        var iMaxHeight = Math.max.apply(null, aItems.map(function (oItem) {
                            return oItem.getDomRef().clientHeight;
                        }));

                        // В эксплорере по неизвестной причине появляется лишнее пространство 
                        // между текстом и кнопкой компенсируем
                        if (sap.ui.Device.browser.name === "ie" ||
                            sap.ui.Device.browser.name === "ed") {
                            iMaxHeight -= 30;
                        }

                        // задаем ее всем карточкам
                        
                        aItems.forEach(function (oItem) {
                            oItem.$().height(iMaxHeight);
                            // $(oItem.getDomRef()).height("calc(" + iMaxHeight + "px + 7rem)");
                        });
                    });
                });
            }
        }
    });
});