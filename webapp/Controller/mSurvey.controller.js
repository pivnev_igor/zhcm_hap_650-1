sap.ui.define([
    "zhcm_Hap_650/Controller/Survey.controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "../utils/Config",
    "../utils/DataAccess",
    "../utils/Formatter",
], function (baseController, JSONModel, Fragment, Config, DataAccess, Formatter) {
    "use strict";


    return baseController.extend("zhcm_Hap_650.Controller.mSurvey", {
        formatter: Formatter,

        onInit: function () {
            this.getOwnerComponent().getRouter().getRoute("msurvey")
                .attachMatched(this._onRouteMatched, this);
                // .attachMatched(this._onRouteMatchedForTesting, this);
            this._initCommonModels();
            this._switchBetweenCompsListAndSurvey();
        },

        _onRouteMatchedForTesting: function (){
            var oTestModel = new JSONModel();
            // oTestModel.loadData('localService/mockdata/survey_IndSlidersMean.json', {}, false);
            // oTestModel.loadData('localService/mockdata/survey_IndToggleBtnsMean.json', {}, false);
            // oTestModel.loadData('localService/mockdata/survey_IndDropdown_CompToggle5.json', {}, false);
            // oTestModel.loadData('localService/mockdata/survey_IndDnd_CompSlider.json', {}, false);
            this._parseSurveyData(oTestModel.getData(), true);
        },

        _bindResponseParser: function(){
            return this._parseSurveyData.bind(this, false);
        },

        _switchBetweenCompsListAndSurvey: function(){
            var bCurrentMode = this.getView().getModel('viewData').getProperty('/compsListVisible');
            this.getView().getModel('viewData').setProperty('/compsListVisible', !bCurrentMode);
        },

        _getIndicatorsFragment: function (oCompContext) {
            var sScale = oCompContext.getModel().getProperty("/IndicatorScale/SCALE_TYPE"),
                sFragmentName = Config.indicatorsFragmentMobile[sScale],
                oFragment = sap.ui.xmlfragment(sFragmentName, this);

            return oFragment;
        },

        _getCompetenceFragment: function (oCompContext) {

            var sScale = oCompContext.getModel().getProperty("/CompetencyScale/SCALE_TYPE"),
                sFragmentName = Config.competenceFragmentMobile[sScale];
            var oFragment = sap.ui.xmlfragment(sFragmentName, this);
            return oFragment;

        },

        onCompetencePress: function (oEvt) {
            this._switchBetweenCompsListAndSurvey();
            this._composeUIForCompetencyAppraisal(oEvt.getSource().getBindingContext("SurveyData"));
            this._triggerCompIconFormatter();
        },

        onGoBackToCompsList: function (oEvt) {
            this._switchBetweenCompsListAndSurvey();
        }
    });
});


