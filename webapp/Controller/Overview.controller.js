sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "../Utils/Formatter",
    "../Utils/Config",
    "../Utils/DataAccess"
], function (JSONModel, Fragment, Controller, MessageBox, Formatter, Config, DataAccess) {
    "use strict";
    return Controller.extend("zhcm_Hap_650.Controller.Overview", {

        formatter: Formatter,
        filterMemo: {},
        _dialogs: [],

        onInit: function () {

            if(this._switchViewIfMobile()){
                return;
            }

            this.getOwnerComponent().getRouter().getRoute("overview")
                .attachMatched(this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mOverview")
                .attachMatched(this._onRouteMatched, this);

            var viewData = {
                displayСompleted: true,
                SubordinatesTabVisible: false,
                MyMarksTabVisible: false,
                ReportTabVisible: false,
                activeTab: 'MyTasks'
            };

            var oViewModel = new JSONModel(viewData);
            this.getView().setModel(oViewModel, "viewData");

            var oYears = new JSONModel(this._getYears(2017, new Date().getFullYear()));
            this.getView().setModel(oYears, "Years");

            // ModelDialog
            var oModelDialog = new JSONModel();
            this.getView().setModel(oModelDialog, "ModelDialog");

        },

        _onRouteMatched: function () {

            var that = this;
            if (this.filterMemo.hasOwnProperty("PernrMemId")) {
                this.getView().setBusy(true);
                DataAccess.getMyTasks(this.filterMemo.PernrMemId)
                    .then(
                        function (oMyTasksResult) {
                            that._bindJSONModel("MyTasks", oMyTasksResult);
                            that.getView().setBusy(false);
                        },
                        function (oError) {
                            that.getView().setBusy(false);
                        });
            } else {

                this._initialDataRead();
            }
        },

        _initialDataRead: function () {
            var that = this,
                setBusy = function (bVal) {
                    this.getView().setBusy(bVal);
                }.bind(this);

            setBusy(true);
            
            this._updateChipData();

            DataAccess.getUserMemId()
                .then(function (aPernr) {
                    var sPernrMemId = aPernr[0].PERNR_MEMID,
                        sPernr = aPernr[0].DESCR.match(/[0-9]{8}/g)[0];

                    that._setAppData("/CurrentPernrMemId", sPernrMemId);
                    that._setAppData("/CurrentPernr", sPernr);
                    that._bindJSONModel("PernrsMemIdSet", aPernr);
                    // lets set initial filter values - representing keys that was used for initial data read of those tabs
                    that._setInitialFilterMemo(sPernrMemId);
                    that.restoreFilterValueByTabs("MyTasks"); //because MyTasks is initial tab

                    return that._getOverviewTabsData(sPernrMemId);

                })
                .then(function (oTasksResult, oMarksResult, oSubordResults) { // success
                        setBusy(false);
                    },
                    function () { // error
                        setBusy(false); // no error handling
                    });

        },

        _updateChipData: function () {
            var oViewModel = this.getView().getModel("viewData");
            DataAccess.getChipData()
                .then(function (aChipData) {
                    oViewModel.setProperty("/TasksAmount", aChipData[0].TASKS_AMOUNT);
                    // в PERNRS_AM_LIST приходит строка вида [{"11111111":19 },{"22222222":1 }],
                    oViewModel.setProperty("/PernrsAmList", JSON.parse(aChipData[0].PERNRS_AM_LIST));
                });
        },

        _setInitialFilterMemo: function (sPernrMemId) {
            this.filterMemo = {
                PernrMemId: sPernrMemId,
                Year: "",
                ProcStatus: ""
            };
        },

        _switchViewIfMobile: function () {
            if (this.getOwnerComponent().getModel("device").getProperty('/isPhone')
                && this._isCurrentViewForDesktop()) {
                this.getOwnerComponent().getRouter().navTo('mOverview', null, null, true);
                return true;
            }
        },

        _isCurrentViewForDesktop: function(){
            return this.getView().getViewName().split('.').slice(-1)[0] === 'Overview'
            ? true
            : false;
        },

        _getOverviewTabsData: function (sPernrMemId) {
            var bTabVisible,
                that = this;

            var getMyTasks = DataAccess.getMyTasks(sPernrMemId).done(
                function (oMyTasksResult) {
                    that._bindJSONModel("MyTasks", oMyTasksResult);
                }
            );
            var getMyMarks = DataAccess.getMyMarks(sPernrMemId).done(
                function (oMyMarksResult) {
                    that._bindJSONModel("MyMarks", oMyMarksResult);
                    bTabVisible = oMyMarksResult.length > 0 ? true : false;
                    that.getView().getModel("viewData").setProperty("/MyMarksTabVisible", bTabVisible);
                }
            );
            var getSubordRatings = DataAccess.getMySubordinates(sPernrMemId).done(
                function (aSubordResults) {
                    bTabVisible = false;
                    if (aSubordResults.length > 0) {
                        that._bindJSONModel("MySubordinates", aSubordResults);
                        that._populateSubordFilter(aSubordResults);
                        that.filterMemo.subordsFullResult = aSubordResults;
                        bTabVisible = true;
                    }
                    that.getView().getModel("viewData").setProperty("/SubordinatesTabVisible", bTabVisible);
                }
            );

            DataAccess.getMyReports(sPernrMemId).done(
                function (aReportResults) {
                    bTabVisible = aReportResults.length > 0 ? true : false;
                    that.getView().getModel("viewData").setProperty("/ReportTabVisible", bTabVisible);
                    $.each(aReportResults, function(sIndex, oRep ) {
                        oRep.PERNR = '';
                        oRep.ID_EVAL_PROC = '';
                    });
                    that._bindJSONModel("MyReports", aReportResults);
                }
            );


            return $.when(getMyTasks, getMyMarks, getSubordRatings);
        },


        _getYears: function (iStartYear, iCurrYear) {
            var aYears = [];
            do {
                aYears.push(iStartYear);
                iStartYear += 1;
            }
            while (iStartYear <= iCurrYear + 1);

            return aYears;
        },

        _bindJSONModel: function (sModelName, oData) {
            var oModel = this.getView().getModel(sModelName);
            if (!oModel) {
                oModel = new JSONModel(oData);
                this.getView().setModel(oModel, sModelName);
            } else {
                oModel.setData(oData);
            }

        },

        _setAppData: function (sPropName, Value) {
            this.getOwnerComponent().getModel("AppData").setProperty(sPropName, Value);
        },

        _getAppData: function (sProperty) {
            return this.getOwnerComponent().getModel("AppData").getProperty(sProperty);
        },

        _populateSubordFilter: function (oSubordRes) {
            var aUnqiquePernrs = [];
            var aUniqueSubords = [];
            $.each(oSubordRes, function (iInd, oPanel) {
                $.each(oPanel.ToApSubrTable.results, function (iInd, oSubordinate) {
                    if (aUnqiquePernrs.indexOf(oSubordinate.PERNR) == -1) {
                        aUniqueSubords.push(oSubordinate);
                        aUnqiquePernrs.push(oSubordinate.PERNR);
                    }
                });
            });

            this._bindJSONModel("Subordinates", aUniqueSubords);
        },

        onApplyFilters: function () {
            // if PernrMemId or year changed -> reread data and save filter values
            var sSelectedTabKey = this.getView().byId("IconTabBar").getSelectedKey(),
                mFilterControls = this._getFilterControls(),
                sPernrMemId = mFilterControls.selPernrMemId.getSelectedKey(),
                sPernr,
                sYear = mFilterControls.selYear.getSelectedKey() || mFilterControls.selYear.getValue(),
                sProc = mFilterControls.selProcStatus.getSelectedKey(),
                oView = this.getView();

            if (mFilterControls.selSubord) {
                var sSubord = mFilterControls.selSubord.getSelectedKey();
            }

            this.filterMemo.ProcStatus = sProc;
            this.filterMemo.Year = sYear;

            // Overview table holds data only for single combination of PERNR_MEMID and YEAR, so we re-read
            if (this.filterMemo.PernrMemId !== sPernrMemId) {
                this.filterMemo.PernrMemId = sPernrMemId;
                sPernr = this.getView().getModel("PernrsMemIdSet").getData()
                    .filter(function (oPernr) { return oPernr.PERNR_MEMID === sPernrMemId; })[0].DESCR.match(/[0-9]{8}/g)[0];

                this._setAppData("/CurrentPernr", sPernr);
                this._setAppData("/CurrentPernrMemId", sPernrMemId);
                this.getView().setBusy(true);
                this._getOverviewTabsData(sPernrMemId)
                    // .done(function () { oView.setBusy(false); }.bind(this));
                    .done(oView.setBusy.bind(oView, false));

                // this.readTabDataWithNewKeys(sSelectedTabKey, sPernrMemId, sYear, sSubord);
                var bReReadOccured = true;
            }

            var aFilters = [];

            if (sProc) {
                aFilters.push(new sap.ui.model.Filter("STATUS_EVAL_PROC", sap.ui.model.FilterOperator.EQ, sProc));
            }
            if (sYear) {
                aFilters.push(new sap.ui.model.Filter("YEAR", sap.ui.model.FilterOperator.EQ, sYear));
            }

            Config.overviewContainerTabs.forEach(function (containerName) {
                var oViewContainer = this.getView().byId(containerName),
                    b = oViewContainer.getBinding('content');
                if (b) {
                    b.filter(aFilters);
                }

            }.bind(this));

            if (sSubord && !bReReadOccured) {
                // if it occured - filtering already took place in oData callback(readTabDataWithNewKeys)
                var aFiltered = this._applyManualSubordinateFilter(sSubord, this.filterMemo.MySubordinates.fullResult);
                this._bindJSONModel(sSelectedTabKey, aFiltered);
            }

            this._updateChipData();
        },


        _getFilterControls: function () {
            var oSelPernrMemId = this.getView().byId("selPernrMemId") || sap.ui.getCore().byId("selPernrMemId");
            var oSelYear = this.getView().byId("selYear") || sap.ui.getCore().byId("selYear");
            var oSelProcStatus = this.getView().byId("selProcStatus") || sap.ui.getCore().byId("selProcStatus");
            var oSelSubord = this.getView().byId("subordFilter") || sap.ui.getCore().byId("subordFilter");

            if (this._oFilterDialog) { // mobileUI
                oSelPernrMemId = sap.ui.core.Fragment.byId("idFiltersDialog", "selPernrMemId");
                oSelYear = sap.ui.core.Fragment.byId("idFiltersDialog", "selYear");
                oSelProcStatus = sap.ui.core.Fragment.byId("idFiltersDialog", "selProcStatus");
                oSelSubord = sap.ui.core.Fragment.byId("idFiltersDialog", "subordFilter");
            }

            return {
                selPernrMemId: oSelPernrMemId,
                selYear: oSelYear,
                selProcStatus: oSelProcStatus,
                selSubord: oSelSubord
            };
        },

        restoreFilterValueByTabs: function (sSelectedTab) {
            var mFilterControls = this._getFilterControls();
            if (mFilterControls.selPernrMemId) {
                mFilterControls.selPernrMemId.setSelectedKey(this.filterMemo.PernrMemId);
                mFilterControls.selYear.setSelectedKey(this.filterMemo.Year);
                mFilterControls.selProcStatus.setSelectedKey(this.filterMemo.ProcStatus);
            }
        },

        _applyManualSubordinateFilter: function (sSubordPernr, oFullResult) {
            var aFilteredMySubordinates = [];
            $.each(oFullResult, function (iInd, oProcPanel) {
                var aFiltered = oProcPanel.ToApSubrTable.results.filter(
                    function (mEmpl) {
                        if (mEmpl.PERNR === sSubordPernr) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                );
                if (aFiltered.length > 0) {
                    var oFilteredPanel = $.extend({}, oProcPanel);
                    delete oFilteredPanel.ToApSubrTable;
                    oFilteredPanel.ToApSubrTable = {};
                    oFilteredPanel.ToApSubrTable.results = aFiltered;
                    aFilteredMySubordinates.push(oFilteredPanel);
                }
            });

            return aFilteredMySubordinates;

        },

        onIconTabBarSelect: function (oEvt) {
            // construct filter screen
            var sTabId = oEvt.getParameter("key"),
                oFilterContainer = this.byId("FiltersContainer");
            if (oFilterContainer) {
                oFilterContainer.setExpanded(false);
                oFilterContainer.destroyContent();

                if (Config.filterFragments[sTabId]) {
                    var oFilterFragment = sap.ui.xmlfragment(Config.filterFragments[sTabId], this);
                    oFilterContainer.addContent(oFilterFragment);

                    this.restoreFilterValueByTabs(sTabId);
                }
            }

            this.getView().getModel('viewData').setProperty("/activeTab", sTabId);

        },

        onAppraisalItemPress: function (oEvt) {
            var oSrc = oEvt.getSource(),
                oSelectedTask = oSrc.getModel("MyTasks").getProperty(oSrc.getBindingContextPath()),
                sProcPath = oSrc.getParent().getParent().getBindingContext("MyTasks").getPath(),
                oProcedure = oSrc.getModel("MyTasks").getProperty(sProcPath);

            this._setAppData.call(this.oView.getController(), "/TaskTxt", oSelectedTask.TASK_TXT);
            this._setAppData.call(this.oView.getController(), "/NameEvalProc", oProcedure.NAME_EVAL_PROC);

            this.onNavigate(this._getNavTargetName(oSelectedTask.NEXT_INTERFACE), {
                "AppraisalId": oSelectedTask.APPRAISAL_ID,
                "IndicatorId": oSelectedTask.INDICATOR_ID
            });

        },

        _getNavTargetName: function(sNextInterface){
            var sMobilePrefix =
                this.getOwnerComponent().getModel("device").getProperty('/isPhone')
                ? 'm' : '';
            return sMobilePrefix + Config.interfaceIds[sNextInterface];
        },

        onNavigate: function (sTarget, oParams) {
            this.getOwnerComponent().getRouter().navTo(sTarget, oParams);
        },

        onMarkIconHelpPress: function (oEvt) {

            var oControl = oEvt.getSource(),
                sScaleBindingPath,
                oPopoverData,
                sFragmentName = "zhcm_Hap_650.fragments.MyMarksScale";

            var fMapMyMarksToPopoverM = function (oMyMarksInput) {
                return {
                    RAT_LOW: oMyMarksInput.RAT_LOW,
                    RAT_HIGH: oMyMarksInput.RAT_HIGH,
                    RAT_DIST: oMyMarksInput.RAT_DIST,
                    RAT_TEXT: oMyMarksInput.RAT_TEXT,
                    Ranges: oMyMarksInput.ToMarkScaleRange.results || []
                };
            };

            var fCreateAndOpenPopover = function (sFragmentName, oEvt, oController) {
                oController._oMarksScalePopover = sap.ui.xmlfragment(sFragmentName, this);
                oController.getView().addDependent(oController._oMarksScalePopover);

                oController._oMarksScalePopover.attachAfterClose(function () {
                    oController._oMarksScalePopover.destroy();
                    oController._oMarksScalePopover = null;
                }, oController);
                oController._oMarksScalePopover.openBy(oEvt.getSource());

            };

            // get certain Panel's marks model, which contains Scale's data
            do {
                oControl = oControl.getParent();
            } while (oControl.getId().indexOf("panel") === -1);

            var oContext = oControl.getBindingContext("MyMarks");
            var oMarksData = oContext.getModel().getProperty(oContext.getPath());


            switch (oMarksData.SCALE_ART) {
                case 'M':
                    sScaleBindingPath = oContext.getPath() + "/ToCommentScaleM/results/0/";
                    sFragmentName += "M";
                    oPopoverData = fMapMyMarksToPopoverM(oContext.getModel().getProperty(sScaleBindingPath));
                    break;
                case 'Q':
                    sScaleBindingPath = oContext.getPath() + "/ToCommentScaleQ/results/";
                    sFragmentName += "Q";
                    oPopoverData = oContext.getModel().getProperty(sScaleBindingPath);
                    break;
            }

            sScaleBindingPath = oContext.getPath() + "/DESCRIPTION";
            var sDescr = oContext.getProperty(sScaleBindingPath);
            if (sDescr && sDescr.length > 0) {
                var lastChr = sFragmentName.toString().slice(-1);
                if (lastChr && (lastChr === "M" || lastChr === "Q")) {
                    sFragmentName = sFragmentName.substring(0, sFragmentName.length - 1);
                }                
                sFragmentName += "C";
                //console.log("<***> sDescr=", sDescr);
                oPopoverData = [
                    { DESCRIPTION: sDescr }
                ];
            }

            this._bindJSONModel("PopoverScale", oPopoverData);
            fCreateAndOpenPopover(sFragmentName, oEvt, this);

        },

        onSubordHelp: function (oEvt) {
            this._oSubordDialog = sap.ui.xmlfragment("zhcm_Hap_650.fragments.SubordValueHelpDialog", this);
            this.getView().addDependent(this._oSubordDialog);
            var oInput = oEvt.getSource();
            this._oSubordDialog.callerInput = oInput;

            var oValueHelpCust = Config.valueHelpCust.filter(function (custItem) {
                return oInput.getId().indexOf(custItem.inputId) > -1;
            })[0];

            var oColModel = new JSONModel({cols: oValueHelpCust.cols});

            var oTable = this._oSubordDialog.getTable();
            oTable.setModel(oColModel, "columns");

            oTable.setModel(this.getView().getModel(oValueHelpCust.model));
            if (oTable.bindRows) {
                oTable.bindRows("/");
            }
            this._oSubordDialog.setKey(oValueHelpCust.key);
            this._oSubordDialog.setDescriptionKey(oValueHelpCust.descrKey);

            if (oInput.getSelectedKey()) {
                this._oSubordDialog.setTokens([
                    new sap.m.Token({
                        key: oInput.getSelectedKey(),
                        text: oInput.getValue()
                    })
                ]);
            }
            
            this._oSubordDialog.update();
            this._oSubordDialog.open();
        },

        onSubordHelpOk: function (oEvent) {
            var aTokens = oEvent.getParameter("tokens");
            this._oSubordDialog.callerInput.setSelectedKey(aTokens[0].getKey());
            this._oSubordDialog.close();
        },

        onSubordHelpCancel: function () {
            this._oSubordDialog.close();
        },

        onSubordHelpAfterClose: function () {
            this._oSubordDialog.destroy();
        },

        onReportHandlePress: function (oEvent) {
            var oSrc = oEvent.getSource();
            var sPath = oSrc.getParent().getBindingContext("MyReports").getPath();
            var oSelectedLinkData = oSrc.getModel("MyReports").getProperty(sPath);
            this._dialogCreate("FormReportDialog", oSelectedLinkData);
        },


        onIndReportFromMarks: function (oEvt) {
            var oCont = oEvt.getSource().getParent().getBindingContext("MyMarks"),
                oPanelData = oCont.getModel().getProperty(oCont.getPath()),
                sAppraisal = oPanelData.APPRAISAL_ID,
                sPernr = oPanelData.PERNR || "00000000",
                sIdEvalProc = oPanelData.ID_EVAL_PROC;

            this._getIndividualReport(sAppraisal, sPernr, sIdEvalProc);
        },

        onIndReportFromSubords: function (oEvt) {
            var oCont = oEvt.getSource().getParent().getBindingContext("MySubordinates"),
                oRowData = oCont.getModel().getProperty(oCont.getPath()),
                sAppraisal = oRowData.APPRAISAL_ID,
                sPernr = oRowData.PERNR || "00000000",
                sIdEvalProc = oRowData.ID_EVAL_PROC;

            this._getIndividualReport(sAppraisal, sPernr, sIdEvalProc);
        },

        _getIndividualReport: function (sAppraisal, sPernr, sIdEvalProc) {
            DataAccess.getReportAnonymityCheck(sPernr, sIdEvalProc, sAppraisal)
                .then(function (oObj) {
                        if (oObj && oObj.FLAG) {
                            DataAccess.getIndividualReport(oObj.PERNR, oObj.ID_EVAL_PROC);
                        } else {
                            MessageBox.show("Недостаточно данных для формирования отчёта. По данной оценке менее 3-х оцененных анкет.", {
                                icon: MessageBox.Icon.INFORMATION,
                                title: "Информация",
                                actions: [MessageBox.Action.OK]
                            });
                        }
                    },
                    function (oError) {
                        console.log("Overview.onIndReportFromMarks('getReportAnonymityCheck') > Error: " + oError);
                    }
                );
        },

        onPrintReportConc: function (oEvt) {
            var oCont = oEvt.getSource().getParent().getBindingContext("MySubordinates"),
                oPanelData = oCont.getModel().getProperty(oCont.getPath()),
                oProcedurePanel = oEvt.getSource().getParent().getParent(),
                aSelectedItems = oProcedurePanel.getContent()[0].getSelectedItems(); // get selected subordinates

            var aSelectedPernrs = aSelectedItems
                .map(function (oItem) {
                    var oRowContext = oItem.getBindingContext('MySubordinates'),
                        oRowData = oRowContext.getModel().getProperty(oRowContext.getPath());
                    return oRowData.PERNR;
                });

            var sCurrentPernr = this.getView().getModel("PernrsMemIdSet").getData()
                .filter(function (m) { return m.PERNR_MEMID === sCurrentPernr; })
                [0].DESCR.match(/[0-9]{8}/)[0];

            DataAccess.getConcReport(aSelectedPernrs, oPanelData.ID_EVAL_PROC, sCurrentPernr);
        },

        _dialogCreate: function (sDialogName, oData) {
            var dialog = this._dialogs[sDialogName],
                oView,
                oModel = this.oView.getModel('ModelDialog');
            oModel.setData(oData);
            if (!dialog) {
                oView = sap.ui.xmlview({
                    viewName: "zhcm_Hap_650.view." + sDialogName
                });
                oView.setModel(oModel, "ModelDialog");
                dialog = oView.getContent()[0];
                this.getView().addDependent(dialog);
                this.getView().setModel(oModel, "ModelDialog");
                this._dialogs[sDialogName] = dialog;
            }
            dialog.open();
        },

        onTBIconFilterPress: function (oEvt) {
            if (!this._oFilterDialog) {
                this._oFilterDialog = sap.ui.xmlfragment("idFiltersDialog", "zhcm_Hap_650.fragments.mobile.OverviewFiltersDialog", this);
                this.getView().addDependent(this._oFilterDialog);
            }
            this._oFilterDialog.open();
        },

        onFiltersClose: function () {
            this._oFilterDialog.close();
        },

        onFiltersDialogApply: function (oEvt) {
            this.onApplyFilters(oEvt);
            this._oFilterDialog.close();
        }

    });
});