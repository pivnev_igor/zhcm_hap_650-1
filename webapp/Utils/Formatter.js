sap.ui.define(["./Config"], function (Config) {
    "use strict";
    var Formatter = {

        activeTasks: function (sCount) {
            var rB = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            if (sCount > 0) {
                return '(' + rB.getText("Overview.ActiveTasks") + " " + sCount + ')'
            } else {
                return ""
            }
        },

        procedureStatus: function (sStatus) {
            var rB = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            switch (sStatus) {
                case "Active":
                    return rB.getText("Overview.Procedure.Status.Active");
                case "Completed":
                    return rB.getText("Overview.Procedure.Status.Completed");
            }
        },

        fioLabel: function (reportKey) {
            var labelText = '';
            var i18nModel = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "i18n/i18n.properties"
            });
            var rB = i18nModel.getResourceBundle();
            if (reportKey === "ZHCM_PM_XXXX_REP_IND") {
                labelText = rB.getText("LabelFIOApre");
            } else {
                labelText = rB.getText("LabelFIO");
            }
            return labelText;
        },

        taskStatusIcon: function (sStatus) {
            switch (sStatus) {
                case Config.indicatorId.inProcess || Config.indicatorId.inProcess2:
                    return "sap-icon://pending";
                case Config.indicatorId.Completed:
                    return "sap-icon://complete";
                case Config.indicatorId.notFilled:
                    return "sap-icon://locked";
                case Config.indicatorId.Denied:
                    return "sap-icon://decline";
            }
        },

        taskStatusColor: function (sStatus) {
            switch (sStatus) {
                case Config.indicatorId.inProcess || Config.indicatorId.inProcess2:
                    return "zRedText";
                case Config.indicatorId.Completed:
                    return "zGreenText";
                case Config.indicatorId.notFilled || Config.indicatorId.Denied:
                    return "zGreyText";
            }

            return ""
        },

        procedureStatusTextColor: function (sStatus) {
            switch (sStatus) {
                case "1":
                    return "zBlueText";
                case "0":
                    return "zGreyText";
            }

            return "";
        },

        onlyActiveVisible: function (displayСompleted, sStatus) {
            if (!displayСompleted && sStatus == Config.indicatorId.Completed) {
                return false;
            } else {
                return true;
            }
        },

        onlyActiveGroupVisible: function (displayСompleted, sStatus, innerResults) {
            var notCompletedTasks = [];
            $.each(innerResults, function (key, element) {
                if (element.INDICATOR_ID != Config.indicatorId.Completed) {
                    notCompletedTasks.push(element.INDICATOR_ID);
                }
            });

            if (!displayСompleted && (sStatus === "0" || !notCompletedTasks.length)) {
                return false;
            } else {
                return true;
            }
        },

        competencyValue: function (Q_value, M_value, CantEstimate, oScale, fAverage) {
            var qvalue = Q_value,
                mvalue = typeof  M_value === 'string' ? Number(M_value) : M_value,
                fAverage = fAverage || 0;
            
                // if (qvalue === "") {
                //     return "";
                // }

            if (CantEstimate) {
                return 'N';
            }

            switch (oScale.DATA_TYPE) {
                case('Q'):
                    return parseInt(qvalue, 10);
                case ('M'):
                    return mvalue.toFixed(1);
                case '': // average
                    return fAverage.toFixed(1);
            }

        },

        sliderEnabled: function (bViewEnabled, bCantEstimate) {
            return (!bViewEnabled || bCantEstimate) ? false : true;
        },

        sliderTextValue: function (mValue, zUnset) {
            return zUnset ? "" : "" + mValue;
        },

        isCompRatingCompleted: function (oCompetence, bInAverage, bRateColleagueTask, sIpsScaleNeutralVal, bIndicOblig, sMinIndicToRate) {
            var bIndicatorsAreRated = true,
                iNotRatedInidcNum = 0,
                iRatedIndicators = 0,
                oEventBus = sap.ui.getCore().getEventBus();

            if (bInAverage === false && //оценку в целом по компетенции не учитываем для среднего
                oCompetence._MARK_FLOAT === 0 &&
                parseInt(oCompetence.MARK) === 0 &&
                oCompetence.CANT_ESTIMATE === false) {
                // на компетенции нет ни оценки, ни "не могу оценить"
                return 'sap-icon://pending';
            }

            if (oCompetence.CANT_ESTIMATE === true) {
                if(oCompetence.COMMENT && oCompetence.COMMENT.length > 0) {
                    oEventBus.publish("isCompRatingCompleted", "iconComplete", oCompetence);
                    return 'sap-icon://complete';
                } else {
                    return 'sap-icon://pending';
                }
            }

            $.each(oCompetence.Indicators, function (iInd, oIndic) {
                if (oIndic.Q_VALUE === sIpsScaleNeutralVal &&
                    oIndic.M_VALUE == 0 &&
                    oIndic.CANT_ESTIMATE == false) {

                    iNotRatedInidcNum += 1;
                }
            });

            if (bIndicOblig && iNotRatedInidcNum > 0) {
                bIndicatorsAreRated = false;
            }            

            if (!bIndicOblig && parseInt(sMinIndicToRate) > 0) {
                iRatedIndicators = oCompetence.Indicators.length - iNotRatedInidcNum;
                bIndicatorsAreRated = iRatedIndicators >= parseInt(sMinIndicToRate);
            }

            if (bIndicatorsAreRated) {
                oEventBus.publish("isCompRatingCompleted", "iconComplete", oCompetence);
                return 'sap-icon://complete';
            } else {
                return 'sap-icon://pending';
            }
        },

        hdrExpandIcon: function (bHdrExpanded) {
            var sIcon;
            bHdrExpanded ? sIcon = 'sap-icon://navigation-right-arrow' : sIcon = 'sap-icon://navigation-down-arrow';
            return sIcon
        },

        photo: function (sPhotoUrl) {
            return sPhotoUrl || "./res/avatar.jpg";
        },

        overviewHeaderWarningVisible: function (
            iActiveTasks,
            aPernrsAmList,
            sCurrentPernr) {
            var bResult = false,
                sPernr;
            if (iActiveTasks !== undefined && aPernrsAmList && sCurrentPernr) {
                bResult = !!aPernrsAmList.reduce(function (acc, oPernr) {
                    sPernr = Object.keys(oPernr)[0];
                    if (sPernr !== sCurrentPernr) {
                        acc += oPernr[sPernr];
                    }
                    return acc;
                }, 0);
            }
            return bResult;
        }
    };

    return Formatter;

}, true);