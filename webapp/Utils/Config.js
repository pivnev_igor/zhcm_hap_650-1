sap.ui.define([], function() {
    'use strict';

    var Config = {
        ZHCM_KPI_CARDS_SERVICE: "/sap/opu/odata/sap/ZHCM_PM_0443_CRDS_LIST_SRV/",
        PM_360_SERVICE: "/sap/opu/odata/sap/ZHCM_PM_0650_SRV/",

        filterFragments: {
            "MyTasks" : "zhcm_Hap_650.fragments.filterFragments.MyTasks",
            "MyMarks" : "zhcm_Hap_650.fragments.filterFragments.MyTasks",
            "MySubordinates" : "zhcm_Hap_650.fragments.filterFragments.Subords"
        },

        taskStatusClosed: ["2", "3", "5"],

        valueHelpCust: [
            {
                inputId: "subordFilter",
                key: "PERNR",
                descrKey: "FIO",
                model: "Subordinates",
                cols: [
                    {label: "ТН", template: "PERNR"},
                    {label: "ФИО", template: "FIO"},
                    {label: "Штатная должность", template: "PLANS_TXT", demandPopin: true},
                    {label: "Организационная единица", template: "ORGEH_TXT", demandPopin: true}
                ]
            }],

        indicatorId: {
            inProcess : '1',
            Completed : '2',
            Denied    : '3',
            InProcess2: '4',
            notFilled : '5'
        },

        overviewContainerTabs: ['contMyTasks','contMyMarks','contMySubordinates'],

        interfaceIds : {
            AD: "addColleagues",
            SU: "survey",
        },

        indicatorsFragmentDesktop: {
            DragNDrop : "zhcm_Hap_650.fragments.IndicatorsDragAndDrop",
            Dropdown : "zhcm_Hap_650.fragments.IndicatorsDropdown",
            Table : "zhcm_Hap_650.fragments.IndicatorsTable",
            Slider : "zhcm_Hap_650.fragments.IndicatorsSliders",
        },

        indicatorsFragmentMobile: {
            DragNDrop : "zhcm_Hap_650.fragments.mobile.survey.Indicators3options",
            Dropdown : "zhcm_Hap_650.fragments.mobile.survey.IndicatorsDropdown",
            Table : "zhcm_Hap_650.fragments.mobile.survey.IndicatorsMeanLess10",
            Slider : "zhcm_Hap_650.fragments.mobile.survey.IndicatorsSlidersMeanMore10",
        },

        competenceFragmentDesktop: {
            Slider: "zhcm_Hap_650.fragments.CompetencySlider",
            Buttons: "zhcm_Hap_650.fragments.CompetencyToggleButtons",
        },

        competenceFragmentMobile: {
            Slider: "zhcm_Hap_650.fragments.mobile.survey.CompetencySlider",
            Buttons: "zhcm_Hap_650.fragments.mobile.survey.CompetencyToggleButtons",
        }


    };

    return Config;
});