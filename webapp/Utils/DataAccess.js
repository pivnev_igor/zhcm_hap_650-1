sap.ui.define(['zhcm_Hap_650/Utils/Config',
        'sap/ui/model/odata/v2/ODataModel'],
    function (Config, oDataModel) {
        'use strict';

        var DataAccess = {
            getPm360Service: function () {
                if (!this._oDataModel360) {
                    this._oDataModel360 = new oDataModel(Config.PM_360_SERVICE, {
                        useBatch: false
                    });
                }

                return this._oDataModel360;
            },

            getInstrText: function (sView, sUiElement, sIdEvalProc) {
                var oInstrData = this.getOwnerComponent().getModel("Instructions").getData().d,
                    sText = "";

                if (oInstrData && oInstrData.hasOwnProperty("results")) {
                    $.each(oInstrData.results, function (iInd, oObj) {
                        if (oObj.INTERFACE_ID === sView &&
                            oObj.UI_ELEMENT === sUiElement &&
                            oObj.ID_EVAL_PROC === sIdEvalProc) {

                            sText = oObj.TEXT;
                        }
                    });

                    return sText;
                }
            },

            _createPernrFilter: function (sPernrMemId) {
                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })
                ];

                return aFlt;
            },

            executeRead: function (oDataModel, sReadUrl, mUrlParams, aFilters) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    var resultData = oData.results || oData;
                    d.resolve(resultData);
                };
                var e = function (oError) {
                    d.reject();
                };
                var mParameters = {
                    urlParameters: mUrlParams,
                    filters: aFilters,
                    success: s,
                    error: e
                };

                oDataModel.read(sReadUrl, mParameters);
                return d.promise();

            },

            // Отличается от executeRead тем, что на выход передается сообщение об ошибке
            executeRead2: function (oDataModel, sReadUrl, mUrlParams, aFilters) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    var resultData = oData.results || oData;
                    d.resolve(resultData);
                };
                var e = function (oError) {
                    d.reject(JSON.parse(oError.responseText));
                };
                var mParameters = {
                    urlParameters: mUrlParams,
                    filters: aFilters,
                    success: s,
                    error: e
                };

                oDataModel.read(sReadUrl, mParameters);
                return d.promise();

            },

            getChipData: function () {
                var sReadUrl = "/ChipSet";
                return this.executeRead(this.getPm360Service(), sReadUrl);
            },

            getUserMemId: function () {

                var oDataModel = new sap.ui.model.odata.v2.ODataModel(Config.ZHCM_KPI_CARDS_SERVICE, {
                    useBatch: false
                });

                var sReadUrl = "/PernrsMemIdSet";
                return this.executeRead(oDataModel, sReadUrl);
            },

            searchForEmpl: function (oSearchString, sAppraisalId, sORGEH) {
                var sReadUrl = "/EmplValueHelpSet";

                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "APPRAISALID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sAppraisalId
                    }),
                    new sap.ui.model.Filter({
                        path: "FIO",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oSearchString
                    }),
                    new sap.ui.model.Filter({
                        path: "ORGEH",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sORGEH
                    })
                ];
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            get_sf_orgeh: function (sAppraisalId) {
                var sReadUrl = "/OrgehValueHelpSet";
                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "APPRAISALID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sAppraisalId
                    })
                ];
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            getMyTasks: function (sPernrMemId) {
                var d = $.Deferred(),
                    sReadUrl = "/MyActPanelSet",
                    aFlt = this._createPernrFilter(sPernrMemId);

                var s = function (oData, oResponse) {
                    d.resolve(oData.results);
                };

                var e = function (oError) {
                    d.reject(oError);
                };

                this.getPm360Service().read(sReadUrl, {
                        urlParameters: {"$expand": "ToMyActTable"},
                        filters: aFlt,
                        success: s,
                        error: e
                    }
                );
                return d.promise();
            },

            getSurveyData: function (sAppraisalId, sPernrMemId, fnParse) {
                var sExpand = "Competencies,Messages,IndicatorScaleValues,CompetencyScaleValues,DescriptionCompScaleValues,DescriptionScaleValues,Competencies/Indicators";
                var that = this;
                this.getPm360Service().metadataLoaded()
                    .then(function () {
                        return that.getPm360Service().createKey("/SurveySet", {
                            PERNR_MEMID: sPernrMemId,
                            APPRAISAL_ID: sAppraisalId
                        });
                    })
                    .then(function (sPath) {
                        return that.executeRead(that.getPm360Service(), sPath, {"$expand": sExpand});
                    })
                    .then(function (data) {
                        fnParse(data, true);
                    });
            },

            getMyMarks: function (sPernrMemId) {
                var sReadUrl = "/MyMarksPanelSet",
                    sExpand = "ToMyMarksTable,ToCommentScaleM,ToCommentScaleQ,ToCommentScaleM/ToMarkScaleRange";

                var aFlt = this._createPernrFilter(sPernrMemId);

                return this.executeRead(this.getPm360Service(), sReadUrl, {"$expand": sExpand}, aFlt);
            },

            getAddedEmployees: function (sAppraisalId, sPernrMemId) {
                var sExpand = "EmplSetTableSet",
                    that = this;

                return this.getPm360Service().metadataLoaded()
                    .then(function () {
                        return that.getPm360Service().createKey("/EmplSetHeadSet", {
                            PERNR_MEMID: sPernrMemId || '',
                            APPRAISAL_ID: sAppraisalId
                        });
                    })
                    .then(function (sPath) {
                        return that.executeRead(that.getPm360Service(), sPath, {"$expand": sExpand});
                    });
            },

            getMySubordinates: function (sPernrMemId) {
                var sReadUrl = "/ApSubrPanelSet",
                    sExpand = "ToApSubrTable";

                var aFlt = this._createPernrFilter(sPernrMemId);

                return this.executeRead(this.getPm360Service(), sReadUrl, {"$expand": sExpand}, aFlt);

            },

            getMyReports: function (sPernrMemId) {
                var sReadUrl = "/ReportSet";

                var aFlt = [
                    new sap.ui.model.Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })
                ];

                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFlt);
            },

            sendSurvey: function (oSurveyData) {
                var d = $.Deferred();

                var fParseMessages = function (aResultMessages) {
                    var aMessages = [],
                        aCompsWithErrors = [],
                        sMsgty;

                    for (var i = 0; i < aResultMessages.length; i++) {
                        sMsgty = aResultMessages[i].TYPE === "E" ? 'Error' : 'Warning';
                        aMessages.push({type: sMsgty, title: aResultMessages[i].TEXT});

                        if (aResultMessages[i].COMP_ID) {
                            aCompsWithErrors.push(aResultMessages[i].COMP_ID);
                        }
                    }
                    return {
                        messages: aMessages,
                        competencies: aCompsWithErrors
                    };
                };

                var s = function (oData, oResponse) {
                    var mParseResult,
                        aMessagesWErrors;
                    if (oData.Messages) {
                        mParseResult = fParseMessages(oData.Messages.results);
                        aMessagesWErrors = $.grep(mParseResult.messages, function (n) {
                            return n.type === 'Error';
                        });
                    }
                    if (aMessagesWErrors && aMessagesWErrors.length > 0) {
                        d.reject(mParseResult);
                    } else {
                        d.resolve(oData);
                    }
                };

                var e = function (oError) {
                    d.reject();
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getPm360Service().create("/SurveySet", oSurveyData, mParameters);
                return d.promise();
            },

            sendEmployeeList: function (data) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    d.resolve(oData);
                };

                var e = function (oError) {
                    d.reject(JSON.parse(oError.responseText));
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getPm360Service().create("/EmplSetHeadSet", data, mParameters);
                return d.promise();
            },

            getReportFIO: function (afilter, entity) {
                var sReadUrl = "/ReportFIOSet";
                if (entity === "ReportFIOApre") {
                    sReadUrl = "/ReportFIOApreSet";
                }
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, afilter);
            },

            getReportProc: function (aFilter) {
                var sReadUrl = "/ReportProcSet";
                return this.executeRead(this.getPm360Service(), sReadUrl, {}, aFilter);
            },

            getIndividualReport: function (sPernr, sEvalProc360, sFormat) {
                sFormat = sFormat || "PDF";
                var sPernrsStringDummy = '';

                var sUrlParams = "REPORTKEY='" + 'ZHCM_PM_XXXX_REP_IND' +
                    "',PERNR='" + sPernr +
                    "',ID_EVAL_PROC='" + sEvalProc360 +
                    "',PERNRS_STRING='" + sPernrsStringDummy +
                    "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_360_SERVICE + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
            },

            getConcReport: function (aPernrs, sEvalProc360, sUserPernr, sFormat) {
                var sPernrsString = "",
                    sUserPernr = sUserPernr || '00000000';
                sFormat = sFormat || "PDF";
                for (var i = 0; i < aPernrs.length; i++) {
                    sPernrsString += aPernrs[i] + '_';
                }
                var sUrlParams = "REPORTKEY='" + 'ZHCM_PM_XXXX_REP_CONS' +
                    "',PERNR='" + sUserPernr +
                    "',ID_EVAL_PROC='" + sEvalProc360 +
                    "',PERNRS_STRING='" + sPernrsString +
                    "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_360_SERVICE + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
            },

            getReportCheck: function(sPernr, sIdEvalProc, sFormatKey, sReportKey) {
                var sUrlParams = "REPORTKEY='" + sReportKey +
                                "',PERNR='" + sPernr +
                                "',ID_EVAL_PROC='" + sIdEvalProc +
                                "',FORMAT='" + sFormatKey +
                                "'";
                var sReadUrl = "/ReportCheckSet(" + sUrlParams + ")";
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, {});
            },

            getReportAnonymityCheck : function (sPernr, sIdEvalProc, sAppraisal) {
                var sUrlParams = "PERNR='" + sPernr +
                    "',ID_EVAL_PROC='" + sIdEvalProc +
                    "',APPRAISAL_ID='" + sAppraisal +
                    "'";
                var sReadUrl = "/ReportAnonymCheckSet(" + sUrlParams + ")";
                return this.executeRead2(this.getPm360Service(), sReadUrl, {}, null);
            },

        };


        return DataAccess;
    });