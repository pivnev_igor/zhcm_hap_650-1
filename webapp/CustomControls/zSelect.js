sap.ui.define(['sap/m/Select',
        'sap/m/library',
        'sap/ui/Device',
        './zSelectList'],
    function(Select, lib, Device, zSelectList) {
        "use strict";

        var zSelect = Select.extend("zhcm_Hap_650.CustomControls.zSelect", {
            metadata : {
                properties : {
                    color : {type : "string", defaultValue : null}
                }
            },

            createList : function() {
                var mListKeyboardNavigationMode = lib.SelectListKeyboardNavigationMode,
                    sKeyboardNavigationMode = Device.system.phone ? mListKeyboardNavigationMode.Delimited : mListKeyboardNavigationMode.None;

                this._oList = new zSelectList({
                    width: "100%",
                    keyboardNavigationMode: sKeyboardNavigationMode
                }).addStyleClass(this.getRenderer().CSS_CLASS + "List-CTX")
                    .addEventDelegate({
                        ontap: function(oEvent) {
                            this._checkSelectionChange();
                            this.close();
                        }
                    }, this)
                    .attachSelectionChange(this.onSelectionChange, this);
                return this._oList;
            },

            onSelectionChange : function(oControlEvent) {
                Select.prototype.onSelectionChange.apply(this, arguments);
                var sColor = oControlEvent.getParameters().selectedItem.getColor();
                var oObj = $("#" + this.getId() + '-label');
                oObj.css('color', sColor);
            },

            renderer: {},

            onAfterRendering : function() {
                Select.prototype.onAfterRendering.apply(this, arguments);

                var oSelectedItem = this._oList.getSelectedItem();
                if (oSelectedItem !== null && oSelectedItem !== undefined) {
                    var sColor = oSelectedItem.getColor();
                    if(sColor) {
                        var oObj = $("#" + this.getId() + '-label');
                        oObj.css('color', sColor);
                    }
                }
            }

        });


        return zSelect;

    });