sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";
    var  IndicatorTile = Control.extend("zhcm_Hap_650.CustomControls.IndicatorTile", {
        metadata : {
            properties: {
                indicId:   {type: "string"},
                text:      { type: "string"},
                rated:     {type: "string"},
                draggable: {type: "boolean", defaultValue: true},
                cantEstimate: {type: "boolean"},
            },
            aggregations: {},
            events: {}
        },

        renderer : function (oRM, oControl) {
            if(oControl.getDraggable()){
                oRM.write("<div draggable='true'");
            } else {
                oRM.write("<div draggable='false'");
            }
            oRM.writeControlData(oControl);
            oRM.addClass("zhcm_Hap_650IndicatorTile");
            oRM.writeClasses();
            oRM.write(">");
            oRM.write(oControl.getText());
            oRM.write("</div>");
        },

    });

    return IndicatorTile;
});