sap.ui.define([
    'sap/ui/core/UIComponent',
    'sap/ui/model/json/JSONModel',
    'zhcm_Hap_650/localService/mockserver'
], function(UIComponent, JSONModel, MockServer) {
    'use strict';

    return UIComponent.extend('zhcm_Hap_650.Component', {

        metadata: {
            manifest: 'json'
        },

        init: function () {
            var deviceModel = new JSONModel({
                isTouch : sap.ui.Device.support.touch,
                isNoTouch : !sap.ui.Device.support.touch,
                isPhone : sap.ui.Device.system.phone,
                isNoPhone : !sap.ui.Device.system.phone,
                listMode : sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
                listItemType : sap.ui.Device.system.phone ? "Active" : "Inactive"
            });
            deviceModel.setDefaultBindingMode("OneWay");
            this.setModel(deviceModel, "device");

            var oAppData = new JSONModel();
            this.setModel(oAppData, "AppData");

            UIComponent.prototype.init.apply(this, arguments);
            this.getRouter().initialize();

            // MockServer.init();

        }
    });
});